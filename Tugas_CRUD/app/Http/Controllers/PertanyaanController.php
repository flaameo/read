<?php

namespace App\Http\Controllers;

use App\Tanya as AppTanya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\tag;

class PertanyaanController extends Controller
{
    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);
        //  $query = DB::table('pertanyaan')->insert([
        //      "judul" => $request["judul"],
        //      "isi" => $request["isi"]
        //  ]);
        //  $tanya = new AppTanya;
        //  $tanya->judul = $request["judul"];
        //  $tanya->isi = $request["isi"];
        //  $tanya->save();
            $pertanyaan = AppTanya::create([
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                "user_id" => Auth::id()
            ]);
            $tags_arr = explode(',',$request["tags"]);
            // dd($tags_arr);
            $tag_ids = [];
            foreach($tags_arr as $tags_name){
                $tag = Tag::firstOrCreate(['tag_name' => $tags_name]);
                $tag_ids[] = $tag->id;
                // $tag = Tag::where("tag_name", $tags_name)->first();
                // if ($tag) {
                //     $tag_ids[] = $tag->id;
                // }else{
                //     $new_tag = Tag::create(["tag_name" => $tags_name]);
                //     $tag_ids[] = $new_tag->id;
                // }
            }
            $pertanyaan->tags()->sync($tag_ids);
        // dd($tag_ids);
        // $user = Auth::user();
        // $user->pertanyaan()->create([
        //     "judul" => $request["judul"],
        //     "isi" => $request["isi"]
        // ]);
        // // $user = Auth::user();
        // $user->pertanyaan()->save($pertanyaan);
        return redirect('/Tanya')->with('sukses', 'berhasil');
    }

    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();
        $user = Auth::user();
        $pertanyaan = AppTanya::all();
        $pertanyaan = $user->pertanyaan;
        return view('pertanyaan.index', compact('pertanyaan'));;
    }

    public function show($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = AppTanya::find($id);
        //  dd($pertanyaan->user);
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        $pertanyaan = AppTanya::find($id);
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        //    $request->validate([
        //        'judul' => 'required|unique:pertanyaan',
        //        'isi' => 'required',
        //    ]);
        //   $query = DB::table('pertanyaan')
        //       ->where('id', $id)
        //       ->update([
        //           'judul' => $request["judul"],
        //           'isi' => $request["isi"]
        //       ]);
          $Tanya = AppTanya::where("id", $id)->update([
              'judul' => $request["judul"],
               'isi' => $request["isi"],
          ]);
        return redirect('/Tanya');
    }

    public function destroy($id)
    {
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        AppTanya::destroy($id);
        return redirect('/Tanya');
    }
}
