<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/master', 'HomeController@index');
// route::get('/pertanyaan', 'PertanyaanController@index');
// route::get('/pertanyaan/create', 'PertanyaanController@create');
// route::post('/pertanyaan', 'PertanyaanController@store');
// route::get('/pertanyaan/{pertanyaan_id}/edit', 'PertanyaanController@edit');
// route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
// route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
 route::delete('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy');
// Route::resource('Tanya', 'TanyaController');
Route::resource('Tanya', 'PertanyaanController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
