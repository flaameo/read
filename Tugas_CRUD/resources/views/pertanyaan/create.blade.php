@extends('master')
@section('content')
<h2 style="padding-left: 37px">Tambah Pertanyaan</h2>
<form action="{{ route('Tanya.index') }}" method="POST">
    @csrf
    <div class="form-group" style="padding-left: 37px">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group" style="padding-left: 37px">
        <label for="body">Isi</label>
        <textarea name="isi" id="body" class="form-control" cols="30" rows="10" placeholder="isi pertanyaan"></textarea>
        @error('body')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group" style="padding-left: 37px">
        <label for="tags">
            Tags
        </label>
        <input type="text" class="form-control" id="tags" name="tags" value="{{ old('tags', '') }}" placeholder="pisahkan dengan koma contoh:postingan,beritaterkini,update">
    </div>
    <button type="submit" class="btn btn-primary" style="margin-left: 37px">Tambah</button>
</form>
</div>

@endsection
