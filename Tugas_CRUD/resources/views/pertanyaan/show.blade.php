@extends('master')
@section('content')
<h3>Show Pertanyaan</h3>
    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
    <p>User : {{ $pertanyaan->user->name }}</p>
    <div>
        Tags :
        @forelse($pertanyaan->tags as $tag)
        <button class="btn btn-primary btn-sm"> {{ $tag->tag_name }} </button>
        @empty
            No Tags
        @endforelse
    </div>
    <a href="/Tanya" class="btn btn-danger">Back</a>

    @endsection
