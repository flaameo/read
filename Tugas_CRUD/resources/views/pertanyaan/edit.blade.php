@extends('master')
@section('content')
<h4>Edit Pertanyaan</h4>
<form action="{{ route('Tanya.update', ['Tanya' => $pertanyaan->id]) }}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" class="form-control" name="judul" id="title" value="{{$pertanyaan->judul}}" placeholder="Masukkan Title">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="isi">Isi</label>
        <textarea name="isi" id="description" class="form-control" cols="30" rows="10"> {{$pertanyaan->isi}} </textarea>
        @error('isi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
</div>

@endsection
{{-- pertanyaan/{{$pertanyaan->id}} --}}
